﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hockey
{
	public interface ICommand
	{
		public bool Exec(int value1, int value2);
	}
}
