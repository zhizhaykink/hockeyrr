﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hockey
{
	public abstract class Sportsman
	{
		public string FullName { get; set; }
		public int Level { get; set; }
		public int Strong { get; set; }
		public int Endurance { get; set; }
		public int Education { get; set; }

	}
}
