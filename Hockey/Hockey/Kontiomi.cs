﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hockey
{
	public class KontiomiIndia : ITeam
	{
		public string Name { get; set; }
		public List<Position> Defender { get; set; } = new List<Position>();
		public List<Position> Attacker { get; set; } = new List<Position>();
		public List<Position> Goalkeeper { get; set; } = new List<Position>();
	}

	public class KontiomiPakistan : ITeam
	{
		public List<Position> Defender { get; set; } = new List<Position>();
		public List<Position> Attacker { get; set; } = new List<Position>();
		public List<Position> Goalkeeper { get; set; } = new List<Position>();
		public string Name { get; set; }
	}
}
