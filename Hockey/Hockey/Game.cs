﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hockey
{
	public abstract class Game
	{
		public const int penalty = 10;
		public abstract void Exec();
		public ITeam Host;
		public ITeam Guests;
	}

	public class Hockey3x3 : Game
	{
		public new const int penalty = 10;
		
		public override void Exec()
		{

		}
	}
}
