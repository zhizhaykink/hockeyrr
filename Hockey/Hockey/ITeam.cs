﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hockey
{
	public interface ITeam
	{
		public List<Position> Defender { get; set; }
		public List<Position> Attacker { get; set; }
		public List<Position> Goalkeeper { get; set; }
		public string Name { get; set; }
	}
}
